# Overview

Install or upgrade to the latest stable version 1.8 of [ISC KEA](https://www.isc.org/kea/) DHCP.

This playbook will deploy KEA DHCP in High Availability mode, so you need exactly 2 hosts in your inventory. This playbook assumes the servers have already been set with the correct network details.



# Features

The playbook will install a High Available cluster of 2 KEA DCHP servers.

- Install Pre-required packages for KEA 1.8
- Install KEA 1.8 repositories and KEA 1.8 packages
- Deploy KEA DHCP and KEA Controller Agent configuration files
- Enable and start services for KEA DHCP and KEA Controller Agent

The playbook also comes with settings for testing using Molecule



# Configuration

The KEA-DHCP role comes with 2 templates for KEA configurations:

- kea-dhcp4.conf.j2 
- kea-crtl-agent.conf.j2

You can adapt those templates for your test or production environment, feel free to check the [official documentation](https://downloads.isc.org/isc/kea/1.8.2/doc/html/index.html) 

Since [KEA High Availability](https://downloads.isc.org/isc/kea/1.8.2/doc/html/arm/hooks.html#ha-high-availability) offers 3 modes (load-balancing, hot-standby, backup), you can choose the mode that you want by using this variable `ha_mode` (the default mode is hot-standby).

You can leverage ansible group variables if you want to have a more dynamic templates.



 # Example Usage

## Configuration

Templates for configuration files

> roles/kea-dhcp/templates/etc/kea/kea-dhcp4.conf.j2
>
> roles/kea-dhcp/templates/etc/kea/kea-ctrl-agent.conf.j2



The `kea-dhcp4.conf.j2` template contains some variables such as `ha_mode` if you want change the HA mode.

To check default variables and custom variables

> roles/kea-dhcp/defaults/main.yml
>
> roles/kea-dhcp/vars/main.yml



Example of inventory (inventory-dhcp)

```ini
--
[dhcp]
server1 type=primary
server2 type=secondary
```



## Run playbook

```bash
ansible-playbook -i inventory-dhcp kea-dhcp4.yml
```



# Code Testing

Molecule testing can run locally within WSL2

Make sure you have your molecule environment installed and operational ([procedure](https://github.com/kenrui-group/ansible-bind#development-environment-setup)) 

## Run molecule

```bash
cd roles/kea-dhcp

# to clean your env in case
molecule destroy

# To create you molecule infra and deploy your ansible role 
molecule converge

# To very after deploying your role
molecule verify
```



# Disclaimer 

To make the HA mode work, we need to expose the service `kea-controller-agent` which is an HTTP API, as you may noticed, it's not HTTPS. If we want to add a layer of security we could install a reverse proxy like Nginx on the DHCP server with SSL. 
